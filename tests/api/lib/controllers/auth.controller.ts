import { ApiRequest } from '../request';
export class AuthController {
  async authenticateUser(body) {
    const response = await new ApiRequest()
      .prefixUrl("https://knewless.tk/api/")
      .method("POST")
      .url(`auth/login`)
      .body(body)
      .send();
    return response
  }
}

