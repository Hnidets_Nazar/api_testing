import { ApiRequest } from '../request';


export class AuthorController {
  tokenValue;
  async getSettings() {
    const response = await new ApiRequest()
      .prefixUrl("https://knewless.tk/api/")
      .method("GET")
      .url(`author`)
      .bearerToken(this.tokenValue)
      .send();
    return response;
  }
  
  async setSettings(body) {
    const response = await new ApiRequest()
      .prefixUrl("https://knewless.tk/api/")
      .method("POST")
      .url(`author`)
      .body(body)
      .bearerToken(this.tokenValue)
      .send();
    return response;
  }
  async getPublicAuthor(authorId) {
    const response = await new ApiRequest()
      .prefixUrl("https://knewless.tk/api/")
      .method("GET")
      .url(`author/overview/${authorId}`)
      .bearerToken(this.tokenValue)
      .send();
    return response;
  }
}
