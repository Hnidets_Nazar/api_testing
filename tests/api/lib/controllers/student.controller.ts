import { ApiRequest } from '../request';

export class StudentController {
  tokenValue;
  async getSettings() {
    const response = await new ApiRequest()
      .prefixUrl("https://knewless.tk/api/")
      .method("GET")
      .url(`student/`)
      .bearerToken(this.tokenValue)
      .send();
    return response;
  }
  
  async setSettings(body) {
    const response = await new ApiRequest()
      .prefixUrl("https://knewless.tk/api/")
      .method("POST")
      .url(`student`)
      .body(body)
      .bearerToken(this.tokenValue)
      .send();
    return response;
  }
  async getStudentInfo() {
    const response = await new ApiRequest()
      .prefixUrl("https://knewless.tk/api/")
      .method("GET")
      .url(`student/info`)
      .bearerToken(this.tokenValue)
      .send();
    return response;
  }
}
