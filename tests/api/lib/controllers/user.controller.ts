import { ApiRequest } from '../request';


export class UserController {
  tokenValue;

  async getCurrentUser() {
    const response = await new ApiRequest()
      .prefixUrl("https://knewless.tk/api/")
      .method("GET")
      .bearerToken(this.tokenValue)
      .url(`user/me`)
      .send();

      
    return response
  }
}


    