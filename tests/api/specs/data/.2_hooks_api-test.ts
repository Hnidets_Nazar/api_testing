import { expect } from 'chai';
import { checkStatusCode } from 'tests/helpers/functionsForChecking.helper';
import { AuthController } from 'tests/api/lib/controllers/auth.controller';
import { ArticlesController } from 'tests/api/lib/controllers/articles.controller';
import { UserController } from 'tests/api/lib/controllers/user.controller';
const auth = new AuthController();
const articles = new ArticlesController();
const user = new UserController();


xdescribe('Articles controller | with hooks', () => {
    let accessToken: string, userId: string;
    let articlesCounter: number;

    before(`should get access token and userId`, async () => {
        let response = await auth.authenticateUser('gnidetslawyer@gmail.com', 'NTest 2022');
        checkStatusCode(response, 200);
        accessToken = response.body.accessToken;

        response = await user.getCurrentUser(accessToken);
        checkStatusCode(response, 200);
        userId = response.body.id;
    });

    it(`get articles`, async () => {
        let response = await articles.getArticles(accessToken);
        checkStatusCode(response, 200);

        articlesCounter = response.body.length;
    });

    it(`add article`, async () => {
        let newArticle = {
            authorId: userId,
            authorName: 'Nazar',
            name: 'My second test article',
            text: 'Hey, this is my second test article. Enjoy!',
        };

        let response = await articles.saveArticle(accessToken, newArticle);
        checkStatusCode(response, 200);

        articlesCounter += 1;
    });

    it(`get articles`, async () => {
        let response = await articles.getArticles(accessToken);
        checkStatusCode(response, 200);

        expect(response.body.length).to.be.equal(articlesCounter);
    });

    afterEach(function () {
        console.log('It was a test');
    });
});

