import {
    checkResponseTime,
    checkStatusCode,
    checkResponseBodyStatus,
    checkResponseBodyMessage,
} from '../../helpers/functionsForChecking.helper';
import { AuthController } from '../lib/controllers/auth.controller';
const auth = new AuthController();


xdescribe('Use test data', () => {
    let invalidCredentialsDataSet = [
        { email: 'zoriana.qa.test@gmail.com', password: '      ' },
        { email: 'zoriana.qa.test@gmail.com', password: 'NTest2022 ' },
        { email: 'zoriana.qa.test@gmail.com', password: 'NTest 2022' },
        { email: 'zoriana.qa.test@gmail.com', password: 'admin' },
        { email: 'zoriana.qa.test@gmail.com', password: 'gnidetslawyer@gmail.com' },
        { email: 'zoriana.qa.test @gmail.com ', password: 'NTest2022' },
        { email: 'zoriana.qa.test@gmail.com  ', password: 'NTest2022' },
    ];

    invalidCredentialsDataSet.forEach((credentials) => {
        it(`login using invalid credentials : ${credentials.email} + ${credentials.password}`, async () => {
            let response = await auth.authenticateUser(credentials.email, credentials.password);

            checkStatusCode(response, 401);
            checkResponseBodyStatus(response, 'UNAUTHORIZED');
            checkResponseBodyMessage(response, 'Bad credentials');
            checkResponseTime(response, 3000);
        });
    });
});

