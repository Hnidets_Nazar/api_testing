import {
    checkResponseTime,
    checkStatusCode,
    checkResponseBodyStatus,
    checkResponseBodyMessage,
} from 'tests/helpers/functionsForChecking.helper';
import { AuthController } from 'tests/api/lib/controllers/auth.controller';
const auth = new AuthController();


xdescribe('Without test data', () => {
    it(`login using invalid credentials email: 'gnidetslawyer@gmail.com', password: '      '`, async () => {
        let response = await auth.authenticateUser('gnidetslawyer@gmail.com', '      ');

        checkStatusCode(response, 401);
        checkResponseBodyStatus(response, 'UNAUTHORIZED');
        checkResponseBodyMessage(response, 'Bad credentials');
        checkResponseTime(response, 3000);
    });

    it(`login using invalid credentials email: 'gnidetslawyer@gmail.com', password: '  NTest2022'`, async () => {
        let response = await auth.authenticateUser('gnidetslawyer@gmail.com', '  ZNTest2022');

        checkStatusCode(response, 401);
        checkResponseBodyStatus(response, 'UNAUTHORIZED');
        checkResponseBodyMessage(response, 'Bad credentials');
        checkResponseTime(response, 3000);
    });

    it(`login using invalid credentials email: 'gnidetslawyer@gmail.com', password: 'NTest2022'`, async () => {
        let response = await auth.authenticateUser('gnidetslawyer@gmail.com', 'NTest2022');

        checkStatusCode(response, 401);
        checkResponseBodyStatus(response, 'UNAUTHORIZED');
        checkResponseBodyMessage(response, 'Bad credentials');
        checkResponseTime(response, 3000);
    });

    it(`login using invalid credentials email: 'gnidetslawyer@gmail.com', password: 'gnidetslawyer@gmail.com'`, async () => {
        let response = await auth.authenticateUser('gnidetslawyer@gmail.com', 'gnidetslawyer@gmail.com');

        checkStatusCode(response, 401);
        checkResponseBodyStatus(response, 'UNAUTHORIZED');
        checkResponseBodyMessage(response, 'Bad credentials');
        checkResponseTime(response, 3000);
    });

    it(`login using invalid credentials email: 'gnidetslawyer@gmail.com', password: 'NTest2022'`, async () => {
        let response = await auth.authenticateUser('gnidetslawyer@gmail.com', 'NTest2022');

        checkStatusCode(response, 401);
        checkResponseBodyStatus(response, 'UNAUTHORIZED');
        checkResponseBodyMessage(response, 'Bad credentials');
        checkResponseTime(response, 3000);
    });

    it(`login using invalid credentials email: 'gnidetslawyer@gmail.com', password: 'admin'`, async () => {
        let response = await auth.authenticateUser('gnidetslawyer@gmail.com', 'admin');

        checkStatusCode(response, 401);
        checkResponseBodyStatus(response, 'UNAUTHORIZED');
        checkResponseBodyMessage(response, 'Bad credentials');
        checkResponseTime(response, 3000);
    });

    it(`login using invalid credentials email: 'gnidetslawyer@gmail.com ', password: 'NTest2022'`, async () => {
        let response = await auth.authenticateUser('gnidetslawyer@gmail.com ', 'NTest2022');

        checkStatusCode(response, 401);
        checkResponseBodyStatus(response, 'UNAUTHORIZED');
        checkResponseBodyMessage(response, 'Bad credentials');
        checkResponseTime(response, 3000);
    });
});
